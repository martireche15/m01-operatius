#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Programa que digui quants dies té el mes indicat
# --------------------------------------------------------

case $1 in 
   1|3|5|7|8|10|12)
     echo "el mes $1 té 31 dies"
     ;;

   4|6|9|11)
     echo "el mes $1 té 30 dies"
     ;;

   2)
     echo "el mes $1 té 29 dies"
     ;;

   *)
     echo "$1 no equival a cap mes"

esac 
exit 0











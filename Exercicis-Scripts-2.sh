#! /bin/bash
# @edt ASIX-M01
# Març 2023
#
# Exercicis Scripts 2
# ----------------------------------------------------------------------------------------------
## PROCESSAR ARGUMENTS

# 3) Processar arguments que són matricules:
# a) Llistar les vàlides, del tipus: 9999-AAA.
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el
#    número d’errors (de no vàlides).

for mat in $*
do
  cont=0	
  case "$mat" in
    "^[0-9]{4}-[A-Z]{3}$")
      	echo "$mat és una matrícula vàlida" ;;
    *)
        echo "$mat no és una matrícula vàlida" 
        ((cont++))
  esac
done
exit 0






# 2)Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.

cont=0
for arg in $*
do
  longitud=$(echo "$arg" | wc -c)
  if [ $longitud -ge 4 ]  #wc conta un de més
  then
    ((cont++))
  fi
done
echo "Has introduit $cont arguments amb 3 o més caràcters"
exit 0

# 1)Processar els arguments i mostrar per stdout només els de 4 o més caràcters.

for arg in $*
do
  longitud=$(echo "$arg" | wc -c)
  if [ $longitud -ge 4 ]
  then
    echo "$arg"
  fi
done
exit 0












































#! /bin/bash
# @edt ASIX-M01
# Març 2023
#
# Acumuladors: Opcions o Arguments
# opcions: -a -b -c -d
# -------------------------------------------------------------
opcions=""
args=""

for arg in $*
do
  case $arg in
  "-a"|"-b"|"-c"|"-d")
    opcions="$opcions $arg";;
  *)
    args="$args $arg";;
  esac
done
echo "opcions: $opcions"
echo "arguments: $args"

exit 0

#! /bin/bash
# @edt ASIX-M01
# Març 2023
#
# Exercicis Scripts Basics
# ----------------------------------------------------------------------------------------------
# 7b)Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la
#    mostra, si no no. --> utilitzant grep

while read -r line
do
  if [ \( "$line" | grep -E "[...]{60}" | echo "$?" \) -eq "0" ]       
  then
    echo "$line"
  fi
done

# 10)Fer un programa que rep com a argument un número indicatiu del número màxim de
#    línies a mostrar. El programa processa stdin línia a línia i mostra numerades un
#    màxim de num línies.

MAX=$1
cont=1
while read -r line  
do
  if [ $cont -le $MAX ]
  then
    echo "$cont: $line"
    ((cont++))
  fi  
done
exit 0

# 9)Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el
#   sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra
#   per stderr.

while read -r nom
do
  if [ \( grep -w "$nom" /etc/passwd | "$?" \) -eq "0" ]
  then
    echo "$nom" 
  else
    echo "$nom" 2>> /dev/null 
  fi
done
exit 0
  
# 8)Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema
#   (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per
#   stderr.

for nom in $*
do
  if [ \( grep -w "$nom" /etc/passwd | echo "$?" \) -eq "0" ]
  then
    echo "$nom" 
  else
    echo "$nom" 2>> /dev/null 
  fi
done
exit 0


# 7)Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la
#    mostra, si no no.

while read -r line
do
  longitud=$(echo "$line" | wc -c)
  if [ $longitud -gt 60 ]       # ${#line}
  then
    echo "$line"
  fi
done
exit 0



# 6)Fer un programa que rep com a arguments noms de dies de la setmana i mostra
#   quants dies eren laborables i quants festius. Si l’argument no és un dia de la
#   setmana genera un error per stderr.
#   Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday

laborables=0
festius=0

for dia in $*
do
  case "$dia" in
    "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
      ((laborables++));;
    "dissabte"|"diumenge")
      ((festius++));;
    *)
      echo "ERROR: $dia no és un dia"
  esac
done
echo "Has introduit $laborables dies laborables i $festius dies festius"
exit 0

# 5)Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
while read -r line
do
  echo "$line" | cut -c1-50
done
exit 0



# 4)Fer un programa que rep com a arguments números de més (un o més) i indica per
#    a cada mes rebut quants dies té el més.

for mes in $*
do
  case "$mes" in
    "2")
      dies=28;;
    "4"|"6"|"9"|"11")
      dies=30;;
    *)
      dies=31;;
    echo "El més: $mes, té $dies"
  esac    
done
exit 0

# 3)Fer un comptador des de zero fins al valor indicat per l’argument rebut.
cont=0
valor=$1
while [ $cont -le $valor ]
do 
  echo "$cont"
  ((cont++))
done
exit 0

# 2)Mostar els arguments rebuts línia a línia, tot numerànt-los.
cont=1
for arg in $*
do 
  echo "$cont: $arg"
  ((cont++))
done
exit 0

# 1)Mostrar l’entrada estàndard numerant línia a línia
cont=1
while read -r line
do
  echo"$cont: $line"
  ((cont++))
done
exit 0








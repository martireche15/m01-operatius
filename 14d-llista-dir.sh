#! /bin/bash
# @edt ASIX-M01
# Març 2023
#
# Descripció: fa un 'ls' del directori rebut i dir si és un regular file, simbolic link...etc
# Veificar 1 arg, i que és un dir
# -------------------------------------------------------------

# 1) Validar que només hi ha un arg
if [ $# -eq 0 ]
then 
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir"
  exit $ERR_NARGS
fi


for dir in $*
do
    if ! [ -d $dir ] #Validem si l'argument és un directori
    then 
      echo "ERROR: $dir no és un directori" 1>&2
  
    else
      echo "dir: $dir"
      llista=$(ls $dir)
      for elem in $llista
      do
        if [ -h "$dir/$elem" ]; then
          echo -e "\t$elem és un link"
        elif [ -f "$dir/$elem" ]; then
          echo -e "\t$elem és un regular file"
        elif [ -d "$dir/$elem" ]; then
          echo -e "\t$elem és un directori"
        else
          echo -e "\t$elem és una altra cosa"	
        fi    
      done
    fi
done
exit 0














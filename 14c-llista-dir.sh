#! /bin/bash
# @edt ASIX-M01
# Març 2023
#
# Descripció: fa un 'ls' del directori rebut i dir si és un regular file, simbolic link...etc
# Veificar 1 arg, i que és un dir
# -------------------------------------------------------------

# 1) Validar que només hi ha un arg
if [ $# -ne 1 ]
then 
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir"
  exit $ERR_NARGS
fi

dir=$1

# 2) Vlidar que arg és un directori
if ! [ -d $dir ]
then 
  echo "ERROR: $dir no és un directori"	
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi

# 3) xixa
llista=$(ls $dir)
for elem in $llista
do
  if [ -h "$dir/$elem" ]; then
    echo "$elem és un link"
  elif [ -f "$dir/$elem" ]; then
    echo "$elem és un regular file"
  elif [ -d "$dir/$elem" ]; then
    echo "$elem és un directori"
  else
    echo "$elem és una altra cosa"	
  fi    
done
exit 0














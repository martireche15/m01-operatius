#! /bin/bash
# @edt ASIX-M01
# Març 2023
#
# Acumuladors: Opcions o Arguments
# opcions: -a -b -c / file: -f file / nums: -n num
# -------------------------------------------------------------
opcions=""
args=""
file=""
nums=""

while [ -n "$1" ]
do
  case $1 in
  "-a"|"-b"|"-c")  #-[abc])
    opcions="$opcions $1";;
  "-f")
    file="$file $2"
    shift;;
  "-n")
    nums="$nums $2"
    shift;;
  *)
    args="$args $1"
  esac
  shift
done
echo "opcions: $opcions"
echo "files: $file"
echo "nums: $nums"
echo "arguments: $args"

exit 0
  


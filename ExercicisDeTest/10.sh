#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Indicar si num not in range [25,50]
# 
# -------------------------------------------------------------

#1) Validar que existeix un argument
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 !range[25,50]"
  exit 2
fi

# 2) xixa
num=$1
if [ $num -lt 25 -o $num -gt 50 ]
then
  echo "num $num no està dins del rang[25,50]]"
fi
exit 0


#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Indicar si num no és igual a 10
# 
# -------------------------------------------------------------

#1) Validar que existeix un argument
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 !igual10"
  exit 2
fi

# 2) xixa
num=$1
if [ $num != 10 ]
then
  echo "num $num no és igual a 10"
fi
exit 0


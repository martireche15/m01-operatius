#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Indicar si name no és ni pere, ni marta, ni anna
# 
# -------------------------------------------------------------

#1) Validar que existeix un argument
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 !igual10"
  exit 2
fi

# 2) xixa
name=$1
if [ $name != 'pere' -a $name != 'marta' -a $name != 'anna' ]
then
  echo "name $name no és ni pere, ni marta, ni anna"
fi
exit 0


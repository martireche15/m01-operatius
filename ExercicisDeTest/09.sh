#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Indicar si num in range [0,18],[65,120]
# 
# -------------------------------------------------------------

#1) Validar que existeix un argument
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 range[0,10],[65,120]"
  exit 2
fi

# 2) xixa
num=$1
if [ $num -gt 0 -a $num -lt 10 -o $num -gt 65 -a $num -lt 120 ]
then
  echo "num $num està dins del rang [0,18],[65,120]"
fi
exit 0


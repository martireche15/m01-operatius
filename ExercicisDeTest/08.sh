#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Indicar si num és 10, 15 o 20
# 
# -------------------------------------------------------------

#1) Validar que existeix un argument
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 igual10,15,20"
  exit 2
fi

# 2) xixa
num=$1
if [ $num -eq 10 -o $num -eq 15 -o $num -eq 20 ]
then
  echo "num $num és igual a 10, 15 o 20"
fi
exit 0


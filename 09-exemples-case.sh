#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Exemples case
# --------------------------------------------------------

case $1 in
   d[ltcjv])
     echo "$1 és un dia laborable"
     ;;

   d[sm])
     echo "$1 és festiu"
     ;;

   *)
     echo "aixó ($1) no és un dia" ;;
esac
exit 0

case $1 in
   [aeiou])
     echo "és una vocal"
     ;;
   
   [bcdfghjklmnpqrstvwxyz])
     echo "és una consonant"
     ;;
   
   *)
     echo "és una altra cosa"
     ;;

esac
exit 0

case $1 in
   "pere"|"pau"|"joan")
     echo "és un nen"
     ;;
   "marta"|"anna"|"julia")
     echo "és una nena"
     ;;
   *)
     echo "no binari"
     ;;
esac
exit 0

#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Programa que calcula la suma dels arguments
# -------------------------------------------------------------

#1) Validar que s'introdueixen 1 o més arguments

if [ ! $# -ge 1 ]
then
  echo "Error: numero args incorrecte"
  exit 2
fi

#2) programa

suma=0
for num in $*
do
  suma=$((num+suma))
done
echo "La suma dels valors introduits és $suma"




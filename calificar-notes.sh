#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Programa que indica per cada nota rebuda si és un suspès, aprovat, notable o excel·lent.
# -------------------------------------------------------------

#1) Validar que es rep almenys una nota
if [ ! $# -ge 1 ]
then
  echo "Error: numero args incorrecte"
  exit 2
fi

#2) Programa
for nota in $*
do
  if ! [ $nota -ge 0 -a $nota -le 10 ] ;then
    echo "Error: $nota no entre [0-10]" >> /dev/stderr
  elif [ $nota -lt 5 ] ;then
    echo "Has tret un $nota, Suspés"
  elif [ $nota -lt 7 ] ;then
    echo "Has tret un $nota, Aprovat"
  elif [ $nota -lt 9 ] ;then
    echo "Has tret un $nota, Notable"
  else 
    echo "Has tret un $nota, Excel·lent"
  fi
done











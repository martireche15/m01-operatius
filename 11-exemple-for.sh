#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Descripció: exemples bucle for
# --------------------------------------------------------

# 7) llistar tots els logins numerats
cont=1
LlistatLogins=$(cut -d: -f1 /etc/passwd | sort)
for login in $LlistatLogins
do
  echo "$cont: $login"
  ((cont++))      
done
exit 0

# 7) llista fitxers del directori actiu numerats
cont=1
llistat=$(ls)
for nom in $llistat
do
  echo "$cont: $nom"
  ((cont++))       # sintaxis més senzilla, igual--> cont=$((cont+1))
done
exit 0

# 6) iterar i mostrar la llista d'arguments amb un contador
cont=1
for arg in $*
do
  echo "$cont: $arg"
  cont=$((cont+1))
done
exit 0

# 5) Iterar i mostrar la llista d'arguments
for arg in "$*"   # iterara 1 cop ja que està encapsulat
do
  echo $arg
done
exit 0

# 4) Iterar i mostrar la llista d'arguments
for arg in $*
do
  echo $arg
done
exit 0

# 3) Iterar pel valor d'una variable
llistat=$(ls)
for nom in $llistat
do
  echo "$nom"
done
exit 0

# 2) Iterar per un conjunt d'elements
for nom in "pere marta pau anna"
do 
  echo "$nom"
done
exit 0

# 1) Iterar per un conjunt d'elements
for nom in "pere" "marta" "pau" "anna"
do 
  echo "$nom"
done
exit 0






